# Teoria dos Grafos - Quarta

> Datas importates
>
> - **09/11** - _Avaliação Dissertativa_
> - **16/11** - _Apresentação Projeto Prático_
> - **23/11** - _Entrega APS_
> - **30/11** - _Semana de TI_
> - **07/12** - _Avaliação N2_
> - **14/12** - _Avaliação SUB_

> Feriados:
>
> - **07/09** - _Feriado_
> - **12/10** - _Feriado_
> - **02/11** - _Feriado_
